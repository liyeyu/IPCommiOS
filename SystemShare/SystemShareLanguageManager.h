//
//  SystemShareLanguageManager.h
//  SystemShare
//
//  Created by 夏祥可 on 2019/1/4.
//  Copyright © 2019 wsl. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface SystemShareLanguageManager : NSObject

/// key关键字  table string表名  comment 描述
+ (NSString *)stringWithKey:(NSString *)key comment:(NSString *)comment;

@end
