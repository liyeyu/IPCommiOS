//
//  WKInterfaceImage+WKImageUrl.m
//  WatchApp Extension
//
//  Created by kiveen on 2019/5/24.
//  Copyright © 2019 wsl. All rights reserved.
//

#import "WKInterfaceImage+WKImageUrl.h"

@implementation WKInterfaceImage (WKImageUrl)

- (void)wkSetImageUrl:(NSString *)imageUrl{
    [[[NSURLSession sharedSession] dataTaskWithURL:[NSURL URLWithString:imageUrl] completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
        if (data != nil && error == nil) {
            UIImage *image = [UIImage imageWithData:data scale:0.3];
            dispatch_async(dispatch_get_main_queue(), ^{
                [self setImage:image];
            });
        }
    }] resume];
    
}

@end
