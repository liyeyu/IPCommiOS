//
//  OpenWalletNetManager.h
//  OpenPlanet
//
//  Created by 谢虎、王胜利 on 2018/6/9.
//  Copyright © 2018年 wsl. All rights reserved.
//  钱包接口请求类
//

#import <Foundation/Foundation.h>
#import "Constant.h"
#import "WalletHeader.h"
#import "WalletModel.h"
#import "WalletUtil.h"
#import "BTCModel.h"
#import "NetManager.h"


@interface WalletNetManager : NSObject

/// 获取链列表
+ (void)chainListWithSuccess:(SuccessBlock)success failure:(FailBlock)failure;

/**
 维护用户代币关系
 
 @param chainId 代币所属链类型
 @param tokenID 代币编号
 @param userAddress 用户钱包地址
 @param type 0是取消关注此代币 1关注此代币
 */
+ (void)walletHttpForUpdateUserTokenWithchainId:(BlockChainId)chainId
                                          tokenID:(NSString *)tokenID
                                      userAddress:(NSString *)userAddress
                                             type:(NSString *)type
                                          success: (SuccessBlock)success
                                          failure:(FailBlock)failure;

/**
 根据链编号与钱包地址获取所有代币列表
 
 @param chainId 代币所属链类型
 @param userAddress 用户钱包地址
 */
+ (void)allTokenWithchainId:(BlockChainId)chainId
                                       userAddress:(NSString *)userAddress
                                           success: (void(^)(NSArray <WalletTokenModel *>*))success
                                           failure:(FailBlock)failure;

/**
 根据链编号与钱包地址获取关注的代币列表（包括代币余额）
 
 @param chainId 代币所属链类型
 @param userAddress 用户钱包地址
 */
+ (void)walletHttpForLikeTokenListWithchainId:(BlockChainId)chainId
                                    userAddress:(NSString *)userAddress
                                        success: (SuccessBlock)success
                                        failure:(FailBlock)failure;



/// 添加钱包
+ (void)addSubWalletWithWalletModel:(WalletModel *)walletModel
                            success: (SuccessBlock)success
                            failure:(FailBlock)failure;

/// 批量添加子钱包
+ (void)addSubWalletsWithWalletModels:(NSArray <WalletModel *> *)walletModels
                              success: (SuccessBlock)success
                              failure:(FailBlock)failure;

/// 获取所有子钱包
+ (void)childWalletsWithMainAddress:(NSString *)mainAddress success:(void(^)(BOOL dataIsRight,NSArray <WalletModel *>*))success fail:(FailBlock)fail;

/**
 更新子钱包信息
 
 @param walletAddress 钱包地址
 @param mainWalletAddress 主钱包地址
 @param otherParams 其他参数
 @param success 更新成功
 @param fail 更新失败
 */
+ (void)updateChildWalletInfoWithWalletAddress:(NSString *)walletAddress
                             mainWalletAddress:(NSString *)mainWalletAddress
                                   otherParams:(NSDictionary *)otherParams
                                       success:(SuccessBlock)success
                                          fail:(FailBlock)fail;


/// 删除子钱包
+ (void)deleteChildWalletWithWalletAddress:(NSString *)walletAddress
                         mainWalletAddress:(NSString *)mainWalletAddress
                                 chainId:(BlockChainId)chainId
                                   success:(SuccessBlock)success
                                      fail:(FailBlock)fail;


/// 更新通讯公钥(臧严科)
+ (void)updateComPublicKey:(NSString *)comPublicKey success:(SuccessBlock)success fail:(FailBlock)fail;
/// 查询通讯私钥(张明成)
+ (void)querySecretComPrivateKeyWithSuccess:(SuccessBlock)success fail:(FailBlock)fail;
/// 更新加密通讯私钥(张明成)
+ (void)updateSecretComPrivateKeyWithPublicKey:(NSString *)publicKey privateKey:(NSString *)privateKey success:(SuccessBlock)success fail:(FailBlock)fail;

/**
 钱包交易记录
 
 @param chainId 代币所属链类型
 @param walletAddress 用户钱包地址
 @param tokenId 用户钱包地址
 @param page 第几页，默认值1
 @param pagesize 每页记录数,默认值25
 @param type 类型1全部2转出3转入4失败
 @param success 成功
 @param fail 失败
 */
+ (void)walletTradeRecordWithchainId:(BlockChainId)chainId
                         walletAddress:(NSString *)walletAddress
                               tokenId:(NSString *)tokenId
                                  page:(NSString *)page
                              pageSize:(NSString *)pagesize
                                  type:(NSString *)type
                               success:(void(^)(NSArray <WalletTradeModel *>*datas))success
                                  fail:(FailBlock)fail;

/// 获取代币详细信息
+ (void)tokenInfoWithchainId:(BlockChainId)chainId
                 walletAddress:(NSString *)walletAddress
                       tokenId:(NSString *)tokenId
                       success:(SuccessBlock)success
                          fail:(FailBlock)fail;

@end

@interface WalletNetManager (Base)

// 网络请求
+ (void)net:(NSString *)localUrl params:(id)params method:(NetMethod)method requestType:(RequestType)requestType success:(SuccessBlock)success fail:(FailBlock)fail;

@end

/// MARK: - PWR相关接口

@interface WalletNetManager (PWR)

/// 查询单钱包账户余额(能量)
+ (void)pwrQueryBalanceWithAddress:(NSString *)address success:(SuccessBlock)success fail:(FailBlock)fail;
/// 查询多账户余额(能量)
+ (void)pwrQueryBalancesWithAddresses:(NSArray <NSString *>*)addresses success:(SuccessBlock)success fail:(FailBlock)fail;
/// 获取银钻数
+ (void)pwrQuerySilverDiamondNumWithSuccess:(SuccessBlock)success fail:(FailBlock)fail;


@end


/// MARK: - BTC相关接口

@interface WalletNetManager (BTC)

/// btc获取单地址余额
+ (void)btcBalanceWithWalletAddress:(NSString *)walletAddress  success:(void(^)(BTCBalanceModel *))success fail:(FailBlock)fail;
/// btc获取多地址余额
+ (void)btcBalanceWithWalletAddresses:(NSArray<NSString *> *)walletAddresses success:(void(^)(NSArray<BTCBalanceModel *>*))success fail:(FailBlock)fail;

/// btc请求UTFOs(未花费的交易)
+ (void)btcUtfosWithWalletAddress:(NSString *)walletAddress  success:(void(^)(NSArray<BTCUTFOModel *>*))success fail:(FailBlock)fail;

/// btc发布交易并保存
+ (void)btcPulishTxSign:(NSString *)sign txhash:(NSString *)txHash toUserId:(NSString *)toUserId success:(SuccessBlock)success fail:(FailBlock)fail;

/// btc获取交易记录
+ (void)btcTradeInfoWithWalletAddress:(NSString *)walletAddress page:(NSUInteger)page pageSize:(NSUInteger)pageSize status:(NSString *)status  success:(void(^)(NSArray <WalletTradeModel *>*))success fail:(FailBlock)fail;
@end

/// MARK: - EOS相关接口

@interface WalletNetManager (EOS)

/// eos恢复钱包通过account获取ownerPublicKey和activePublicKey
+ (void)eosGetAccountWithAccount:(NSString *)account success:(SuccessBlock)success fail:(FailBlock)fail;
/// eos根据owner公钥查询账户
+ (void)eosGetAccountWithOwnerPublicKey:(NSString *)ownerPublicKey activePublicKey:(NSString *)activePublicKey success:(void(^)(NSArray <NSString *>*))success fail:(FailBlock)fail;

/// eos请求余额
+ (void)eosBalanceWithAccount:(NSString *)account success:(SuccessBlock)success fail:(FailBlock)fail;

/// eos查询创建账户ram、new、cpu单价
+ (void)eosGetCreateAccountFeeUnitWithSuccess:(SuccessBlock)success fail:(FailBlock)fail;
/// eos请求Data
+ (void)eosActionDataWithParams:(NSDictionary *)params success:(SuccessBlock)success fail:(FailBlock)fail;
/// eos获取最新区块信息
+ (void)eosGetInfoWithSuccess:(SuccessBlock)success fail:(FailBlock)fail;
/// eos发布交易
+ (void)eosPushTransactionWithParams:(NSDictionary *)params success:(SuccessBlock)success fail:(FailBlock)fail;
/// eos交易历史
+ (void)eosTradeInfoWithWalletAddress:(NSString *)walletAddress page:(NSUInteger)page pageSize:(NSUInteger)pageSize status:(NSString *)status  success:(void(^)(NSArray <WalletTradeModel *>*))success fail:(FailBlock)fail;

@end


/// MARK: - 总资产
@interface WalletNetManager (TotalAsset)
/// 总资产1：是返回列表的，不包括余额以及行情
+ (void)totalAssetExceptBalanceSuccess:(SuccessBlock)success fail:(FailBlock)fail;
/// 总资产2：这个是具体详情的
+ (void)totalAssetDetailsSuccess:(SuccessBlock)success fail:(FailBlock)fail;
@end



/// MARK: - 设备\卡包
@interface WalletNetManager (HDWalletDevice)

/// 添加设备/卡片
+ (void)addDeviceWithId:(NSString *)Id cardInfo:(NSString *)cardInfo success:(SuccessBlock)success fail:(FailBlock)fail;
/// 设备/卡片添加钱包//    accountType（冷钱包类型 1、卡片 2、watch 3、手环 int类型）    不能为空
+ (void)addDeviceHDWalletWithWalletModel:(WalletModel *)wallet accountType:(NSInteger)accountType cardInfo:(NSString *)cardInfo success:(SuccessBlock)success fail:(FailBlock)fail;
/// 设备(包括卡片)列表
+ (void)deviceListWithSuccess:(SuccessBlock)success fail:(FailBlock)fail;
/// 更新设备/卡片信息
+ (void)updateDeviceWithSuccess:(SuccessBlock)success fail:(FailBlock)fail;
/// 删除设备/卡片
+ (void)deleteDeviceWithId:(NSString *)deviceId success:(SuccessBlock)success fail:(FailBlock)fail;
/// 删除设备/卡片里面某个钱包
+ (void)deleteDeviceHDWalletWithAddress:(NSString *)address deviceId:(NSString *)deviceId cardInfo:(NSString *)cardInfo Success:(SuccessBlock)success fail:(FailBlock)fail;
/// 钱包冷热转换 值为1冷钱包转为热钱包，值为3热钱包转为冷钱包
+ (void)walletColdHotExchangeWithWallet:(WalletModel *)wallet cardInfo:(NSString *)cardInfo success:(SuccessBlock)success fail:(FailBlock)fail;
/// 修改冷钱包头像、名称(accountIcon（ 钱包头像）accountName（钱包名称） index（钱包在卡片的位置，int类型）)
+ (void)updateColdWalletInfoWithId:(NSString *)deviceId address:(NSString *)address hdWalletDeviceType:(NSUInteger)hdWalletDeviceType changeParams:(NSDictionary *)changeParams success:(SuccessBlock)success fail:(FailBlock)fail;

@end



@interface WalletNetManager (Chat)

///获取钱包地址和公钥
+ (void)getUserAddressInfoWithUserId:(NSString *)userId success:(SuccessBlock)success fail:(FailBlock)fail;
/// 检查版本更新
+ (void)appVersionCheckWithAppID:(NSString *)AppID success:(SuccessBlock)success fail:(FailBlock)fail;
///欢迎消息
+ (void)appWelcomMessageWithSuccess:(SuccessBlock)success fail:(FailBlock)fail;
///设置微信收款码
+ (void)addWechatQrImage:(NSString *)url success:(SuccessBlock)success fail:(FailBlock)fail;
///设置支付宝收款码
+ (void)addAlipayQrImage:(NSString *)url success:(SuccessBlock)success fail:(FailBlock)fail;
///星际通讯扫码登录
+ (void)tcserverScanLoginWithParams:(NSDictionary *)params success:(SuccessBlock)success fail:(FailBlock)fail;
/// 内控上传影像
+ (void)PLNKUploadImageWithParams:(NSDictionary *)params success:(SuccessBlock)success fail:(FailBlock)fail;

@end


@interface WalletNetManager (TrusteeShip)


/// 托管账户列表
/// @param userId 用户ID
/// @param success 请求成功
/// @param fail 请求失败
+(void)accountListWithUserId:(NSString *)userId
                     success:(SuccessBlock)success
                     fail:(FailBlock)fail;

/// 交易记录
/// @param userId 用户ID
/// @param page 页码
/// @param pageSize 每页有几条记录
/// @param trusteeshipCoinId 代币id
/// @param type 类型
/// @param success 请求成功
/// @param fail 请求失败
+(void)accountTradeHistoryWithUserId:(NSString *)userId
                                page:(NSString *)page
                            pageSize:(NSString *)pageSize
                   trusteeshipCoinId:(NSString *)trusteeshipCoinId
                                type:(NSString *)type
                             success:(SuccessBlock)success
                             fail:(FailBlock)fail;

///  获取资产列表
/// @param userId 用户ID
/// @param success 请求成功
/// @param fail 请求失败
+(void)coinListWittUserId:(NSString *)userId
                  success:(SuccessBlock)success
                  fail:(FailBlock)fail;


/// 增加资产
/// @param userId 用户ID
/// @param trusteeshipCoinId 代币ID
/// @param type 类型
/// @param success 请求成功
/// @param fail 请求失败
+(void)addCoinWithUserId:(NSString *)userId
       trusteeshipCoinId:(NSString *)trusteeshipCoinId
                    type:(NSString *)type
                 success:(SuccessBlock)success
                 fail:(FailBlock)fail;



/// 资产余额
/// @param chainId 链id
/// @param accountAddress 账户地址
/// @param coinAddress  币地址
/// @param coinDecimals 精度值
/// @param success 请求成功
/// @param fail 请求失败
+(void)accountBalanceWithChainId:(NSString *)chainId
                  accountAddress:(NSString *)accountAddress
                     coinAddress:(NSString *)coinAddress
                    coinDecimals:(NSInteger)coinDecimals
                         success:(SuccessBlock)success
                         fail:(FailBlock)fail;


/// 提现
/// @param signedTransactionData 认证交易数据
/// @param userId 用户id
/// @param trusteeshipCoinId 代币id
/// @param value 数量
/// @param txTo 16进制
/// @param success 请求成功
/// @param fail 请求失败
+(void)reflectBalanceWithsignedTranSactionData:(NSString *)signedTransactionData
                                        userId:(NSString *)userId
                             trusteeshipCoinId:(NSString *)trusteeshipCoinId
                                         value:(NSString *)value
                                          txTo:(NSString *)txTo
                                       success:(SuccessBlock)success
                                       fail:(FailBlock)fail;

/// 充值
/// @param userId 用户id
/// @param trusteeshipId 代币id
/// @param signedTransactionData 认证交易数据
/// @param value 数量
/// @param from 来自
/// @param txFrom 16进制来自
/// @param txTo 给
/// @param success 请求成功
/// @param fail 请求失败
+(void)rechargeBalanceWithUserId:(NSString *)userId
                   trusteeshipId:(NSInteger)trusteeshipId
           signedTransactionData:(NSString *)signedTransactionData
                           value:(float)value from:(NSString *)from
                          txFrom:(NSString *)txFrom
                            txTo:(NSString *)txTo
                         success:(SuccessBlock)success
                         fail:(FailBlock)fail;

@end
