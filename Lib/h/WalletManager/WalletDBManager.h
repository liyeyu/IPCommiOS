//
//  OpenWalletDBManager.h
//  OpenPlanet
//
//  Created by 王胜利  on 2018/6/8.
//  Copyright © 2018年 wsl. All rights reserved.
//  钱包数据库操作类
//

#import <Foundation/Foundation.h>
#import "WalletHeader.h"
#import "WalletModel.h"
#import "WalletUtil.h"
@class ACSCardModel;

@interface WalletDBManager : NSObject

/// 创建并更新数据库
+ (void)createWalletDataBase;

@end

@interface WalletDBManager (Wallet)

/// 请求所有钱包
+ (NSArray <WalletModel *>*)queryAllWallets:(BlockChainId)chainId;
/// 请求所有非观察者钱包
+ (NSArray<WalletModel *> *)queryAllNoSeeWallets:(BlockChainId)chainId;
/// 获取当前钱包
+ (WalletModel *)queryCurrentWallet:(BlockChainId)chainId;
/// 根据钱包ID获取钱包
+ (WalletModel *)queryWalletWithWalletAddress:(NSString *)address;
/// 添加钱包
+ (BOOL)addWallet:(WalletModel *)wallet;
/// 移除钱包
+ (void)removeWalletWithWalletAddress:(NSString *)address;
/// 移除某条链下的所有钱包
+ (void)removeAllWalletsWithBlockChainId:(BlockChainId)chainId;
/// 修改当前选中钱包
+ (void)changeCurretWallet:(WalletModel *)wallet;
/// 修改钱包名称
+ (void)changeWalletNameWithAddress:(NSString *)address name:(NSString *)name;
/// 修改钱包头像
+ (void)changeWalletAvatarWithAddress:(NSString *)address avatar:(NSString *)avatar;


@end


@interface WalletDBManager (PWR)

/// 根据owner和active公钥查询EOS钱包
+ (WalletModel *)mainPwrWallet;

@end

@interface WalletDBManager (EOS)

/// 获取所有已激活的EOS钱包
+ (NSArray <WalletModel *>*)queryAllActiveEOSWallets;
/// 修改EOStmpAccount
+ (void)changeEOSTmpAccountWithAddress:(NSString *)address tmpAccount:(NSString *)tmpAccount;
/// 修改EOS账户地址(EOS切换主账户)
+ (void)changeEOSAddressWithOldAddress:(NSString *)oldAddress newAddress:(NSString *)newAddress;
/// 根据owner和active公钥查询EOS钱包
+ (WalletModel *)queryEosWalletWithOwnerPublicKey:(NSString *)ownerPublicKey activePublicKey:(NSString *)activePublicKey;
/// 激活EOS钱包
+ (void)changeEOSWalletToActive:(WalletModel *)wallet;

@end


@interface WalletDBManager (HDWallet)

// 冷钱包创建成功
+ (void)hdWalletCreateSuccess:(NSString *)address hdDeviceId:(NSString *)hdDeviceId index:(NSString *)index;
/// Eth冷钱包转热钱包
+ (void)ETHColdToHotWithAddress:(NSString *)address secretPrivateKey:(NSString *)secretPrivateKey;
/// Eth热钱包转冷钱包
+ (void)ETHHotToColdWithAddress:(NSString *)address hdDeviceId:(NSString *)hdDeviceId index:(NSString *)index;
/// BTC冷钱包转热钱包
+ (void)BTCColdToHotWithAddress:(NSString *)address secretSeedPhrase:(NSString *)secretSeedPhrase;
/// BTC热钱包转冷钱包
+ (void)BTCHotToColdWithAddress:(NSString *)address hdDeviceId:(NSString *)hdDeviceId index:(NSString *)index;

@end



@interface WalletDBManager (HDWallletDevice)

/// 获取所有卡片
+ (NSArray <HDWalletModel *>*)getAllHDWalletCards;
/// 获取所有设备 目前手表
+ (NSArray <HDWalletModel *>*)getAllHDWalletDevices;
/// 添加硬件钱包设备
+ (BOOL)addHDWalletDevice:(HDWalletModel *)hdWalletDevice;
/// 获取某一个设备/卡片
+ (HDWalletModel *)getDeviceWithDeviceId:(NSString *)deviceId;
/// 删除某设备/卡片
+ (BOOL)deleteHDDeviceWithId:(NSString *)Id;
//更新iWatch的信息
//+ (BOOL)updateAppleWatchWithDic:(NSDictionary *)dicInfo;
//读取的watch数据直接转WalletCardModel
+ (HDWalletModel *)getHDWalletModelFromDic:(NSDictionary *)dic;

/// 添加卡片
+ (void)addCard:(ACSCardModel *)cardModel wallets:(NSArray <WalletModel *>*)wallets;

/// 同步卡片
+ (void)syncCard:(NSArray <HDWalletModel *>*)hdwallets;

@end

