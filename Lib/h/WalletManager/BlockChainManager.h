//
//  BlockChainManager.h
//  WalletManager
//
//  Created by 王胜利 on 2018/8/10.
//  Copyright © 2018年 pansoft. All rights reserved.
//  链模型管理类
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

/**
 链

 - BlockChainIdETH:          以太坊
 - BlockChainIdBTC:          比特币
 - BlockChainIdEOS:          EOS
 - BlockChainIdPWR :         涡链
 - BlockChainIdEOSTEST:      EOS测试
 - BlockChainIdACT:          ACT
 - BlockChainIdBCH:          BCH
 - BlockChainIdBTM:          BTM
 - BlockChainIdLTC:          LTC
 - BlockChainIdXRP:          XRP
 - BlockChainIdGXM:          GXM
 - BlockChainIdQTUM:         GTUM
 - BlockChainIdUnknown:      未知
 */
typedef NS_ENUM(NSUInteger, BlockChainId) {
    BlockChainIdBTC                = 1,
    BlockChainIdETH                = 2,
    BlockChainIdEOS                = 3,
    BlockChainIdPWR                = 4,
    BlockChainIdEOSTEST            = 5,
    BlockChainIdACT                = 6,
    BlockChainIdBCH                = 7,
    BlockChainIdBTM                = 8,
    BlockChainIdLTC                = 9,
    BlockChainIdXRP                = 10,
    BlockChainIdGXM                = 11,
    BlockChainIdQTUM               = 12,
    BlockChainIdUnknown            = 1000,
};


/// MARK: - 链 模型

@interface BlockChainModel : NSObject

/// 链类型()
@property (nonatomic, assign) BlockChainId chainId;
/// 链名称
@property (nonatomic, copy)   NSString *chainName;
/// 链全称
@property (nonatomic, copy)   NSString *chainFullName;
/// 链图标(大)
@property (nonatomic, strong) UIImage *chainIcon;
/// 链图标(小)
@property (nonatomic, strong) UIImage *chainSmallIcon;

@end



@interface BlockChainManager : NSObject

/// 根据 '链类型' 获取'链模型'
+ (BlockChainModel *)blockChainByChainId:(BlockChainId)chainId;
/// 根据 '链名称(简称)' 获取'链模型'
+ (BlockChainModel *)blockChainByChainName:(NSString *)name;

@end
